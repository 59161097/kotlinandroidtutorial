package com.burid.kotlinandroidtutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class ListViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_view)
        val arrayAdapter : ArrayAdapter<*>
        val users = arrayOf(
            "virat kohil","Rohi Sharma","Steav Smith","Kane Willison","Ross Taylor",
            "virat kohil","Rohi Sharma","Steav Smith","Kane Willison","Ross Taylor",
            "virat kohil","Rohi Sharma","Steav Smith","Kane Willison","Ross Taylor",
            "virat kohil","Rohi Sharma","Steav Smith","Kane Willison","Ross Taylor"
        )
        val userList = findViewById<ListView>(R.id.userList)
        arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, users)
        userList.adapter = arrayAdapter
    }
}