package com.burid.kotlinandroidtutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_radio_button.*

class RadioButtonActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_radio_button)
        radio_group.setOnCheckedChangeListener{ group, checkId ->
            val radio = findViewById<RadioButton>(checkId)
            Toast.makeText(applicationContext, "On button click " +"${radio.text}",
                Toast.LENGTH_LONG ).show()
        }
        button.setOnClickListener {
            val id = radio_group.checkedRadioButtonId
            if(id!=-1){

            } else {
                Toast.makeText(applicationContext, "On button click " + "nothing select",
                    Toast.LENGTH_LONG ).show()

            }
        }
        
    }
   fun radio_button_click(view: View){
       val radio = findViewById<RadioButton>(radio_group.checkedRadioButtonId)
       Toast.makeText(applicationContext, "On Click: ${radio.text}", Toast.LENGTH_LONG ).show()
   }
}